from django.contrib import admin
from .models import Hats


# Register your models here.
@admin.register(Hats)
class Hats(admin.ModelAdmin):
    list_display = (
        "style_name",
        "fabric",
        "color",
        "picture_url",
    )
