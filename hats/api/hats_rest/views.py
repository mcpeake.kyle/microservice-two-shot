from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hats, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
        ]


# Need to pass in Location data into parameters
class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "location",
        "style_name",
        "fabric",
        "color",
        "picture_url",
    ]
    encoders = {"location": LocationVODetailEncoder()}


@require_http_methods(["GET"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        # GET ALL HATS
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        content['location'] = location_vo_id
        # try:
        #     location_href = f'/api/locations/{location_vo_id}/'
        #     location = LocationVO.objects.get(import_href=location_href)
        #     content['location'] = location
        # except LocationVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid location id"},
        #         status=400,
        #     )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["POST"])
def api_create_hat(request, location_vo_id):
    # POST / CREATE
    content = json.loads(request.body)
    try:
        location_href = f'/api/locations/{location_vo_id}/'
        location = LocationVO.objects.get(import_href=location_href)
        content['location'] = location
    except LocationVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )
    hat = Hats.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat_details(request, pk):
    if request.method == "GET":
        # GET HAT DETAILS
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
        # DELETE
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
        # PUT / UPDATE HAT DETAILS
    else:
        content = json.loads(request.body)
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
