from django.urls import path
from .views import api_list_hats, api_hat_details, api_create_hat


urlpatterns = [
    path('hats/',
         api_list_hats,
         name='api_list_hats'),

    path('create_hat/<int:location_vo_id>/',
         api_create_hat,
         name='api_list_hats'),

    # path('locations/<int:location_vo_id>/',
    #      api_list_hats,
    #      name='api_list_hats'),

    path('hat/<int:pk>/',
         api_hat_details,
         name="api_hat_details"),
]
