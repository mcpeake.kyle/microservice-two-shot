import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import '/'
import NewHatForm from './NewHatForm'
import HatList from './HatList'
import Locations from './Locations'
import CreateLocation from './CreateLocation'

export default function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/hats" element={<HatList />} />
          <Route path="/create_hat" element={<NewHatForm />} />
          <Route path="/locations" element={<Locations />} />
          <Route path="/create_location" element={<CreateLocation />} />

          <Route path="/shoes" />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
