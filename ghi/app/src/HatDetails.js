import React, {useEffect, useState } from 'react';

export default function HatList(props) {
  // const [hats, setHats] = useState([])
  const [hatColumns, setHatColumns] = useState([[], [], []])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    try {
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
            let hat_id = parseInt(hat.href.match(/[0-9]/g).join(""));
            // console.log("hat id is:", hat_id)
            // console.log("hat data is:", hat)

            const detailUrl = `http://localhost:8090/api/hat/${hat_id}`;
            requests.push(fetch(detailUrl));
        }
        const responses = await Promise.all(requests);
        const columns = [[], [], []];
        let i = 0;
        for (const hatResponse of responses) {
            if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {i = 0;}
            }
            else {console.error(hatResponse);}
        }
        setHatColumns(columns);
        }
    } catch (e) {
        console.error(e);
    }
}

function HatColumn(props) {
    return (
    <div className="col">
        {props.list.map(hats => {
            return (
            <div key={hats.hat_id} className="card mb-3 shadow">
                <img src={hats.picture_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">{hats.fabric} {hats.style_name}</h5>
                <h5 className="card-subtitle mb-2 text-muted"></h5>
                </div>
            </div>
            );
        })}
    </div>
    );
  }


  useEffect(() => {
    fetchData()
  }, [])

    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Hats</h1>
          <div className="col-lg-6 mx-auto">
          </div>
        </div>
        <div className="container">
          <div className="row">
            {hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
