import React, { useEffect, useState } from 'react'

export default function NewHatForm() {
    const [formData, setFormData] = useState({
        // fabric: 'Leather',
        // style_name: 'Cowboy Hat',
        // color: 'Tan',
        // picture_url: 'https://i.etsystatic.com/24567885/r/il/665627/4623753021/il_fullxfull.4623753021_th7m.jpg',
        // location: '',
        fabric: '',
        style_name: '',
        color: '',
        picture_url: '',
        location: '',
    })

    const [locations, setLocations] = useState([])

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            // console.log(data.locations)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = `http://localhost:8090/api/create_hat/${formData.location}/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: '',
            })
        }
    }

    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"
                        value={formData.fabric} onChange={handleInputChange} />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"
                        value={formData.style_name} onChange={handleInputChange} />
                        <label htmlFor="style_name">Style name</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input placeholder="Color" required type="text" name="color" id="color" className="form-control"
                        value={formData.color} onChange={handleInputChange} />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control"
                        value={formData.picture_url} onChange={handleInputChange} />
                        <label htmlFor="picture_url">Picture url</label>
                    </div>
                    <div className="mb-3">
                        <select required name="location" id="location" className="form-select" value={formData.location} onChange={handleInputChange}>
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.id}>{location.closet_name}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </main>
            );
}
