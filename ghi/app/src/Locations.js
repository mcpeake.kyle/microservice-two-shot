import React, {useEffect, useState } from 'react';

export default function LocationList(props) {
  const [locations, setLocations] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const {locations} = await response.json();
        setLocations(locations)
    }
}
const handleRemove = async (event) => {
  event.preventDefault()
  let id = event.target.value
//   console.log("id is:", id)
  const url = `http://localhost:8100/${id}`
  const fetchConfig = {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
    }
  }
  const response = await fetch(url, fetchConfig)
  if (response.ok) {
    fetchData()
  }
}

    useEffect(() => {
      fetchData()
    }, [])

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Location name</th>
            <th>Section number</th>
            <th>Shelf number</th>
          </tr>
        </thead>
        <tbody>
          {locations?.map(location => {
            return (
              <tr key={location.href}>
                <td>{ location.closet_name }</td>
                <td>{ location.section_number }</td>
                <td>{ location.shelf_number }</td>
                <td><button value={ location.href } onClick={handleRemove}>Delete location</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
