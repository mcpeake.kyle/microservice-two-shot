import React, {useEffect, useState } from 'react';

export default function HatList(props) {
  // const [hats, setHats] = useState([])
  const [hatColumns, setHatColumns] = useState([[], [], []])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    try {
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
            let hat_id = parseInt(hat.href.match(/[0-9]/g).join(""));
            const detailUrl = `http://localhost:8090/api/hat/${hat_id}`;
            requests.push(fetch(detailUrl));
        }
        const responses = await Promise.all(requests);
        const columns = [[], [], []];
        let i = 0;
        for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              columns[i].push(details);
              i = i + 1;
              if (i > 2) {
                i = 0;
              }
            }
            else {
              console.error(hatResponse);
            }
        }
        setHatColumns(columns);
        }
    } catch (e) {
        console.error(e);
    }
}

const handleRemove = async (event) => {
  event.preventDefault()
  let id = event.target.value
  // console.log(id)
  const url = `http://localhost:8090/${id}`
  const fetchConfig = {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
    }
  }
  const response = await fetch(url, fetchConfig)
  if (response.ok) {
    // send a request to get all the hats/shoes and update the state with that data.
    // const deleteLunchOrder = (lunchItem) => {
    //   // this is where you would go make a DELETE request
    //   // on successful deletion you have two options
    //   // option 1
    //   const lunchItemIdx = lunchOrders.indexOf(lunchItem)
    //   // lunchOrders.slice(lunchItemIdx, 1)
    //   const newLunchOrders = [...lunchOrders]

    //   // or use filter
    //   const newLunchOrder = lunchOrders.filter(item => item.lunchItem !== lunchItem)
    //   setLunchOrders(newLunchOrders)
    // }
    // or
    // filter object out and setState
    // or
    // call fetchData()
    fetchData()
    // Don't use "window.location.reload()"
  }
}

function HatColumn(props) {
    return (
    <div className="col">
        {props.list.map(hats => {
            return (
            <div key={hats.href} className="card mb-3 shadow">
                <img src={hats.picture_url} className="card-img-top" />
                <div className="card-body">
                <div>
                  <h5 className="card-title">{hats.color} {hats.fabric} {hats.style_name}</h5>
                </div>
                <div>
                  <h7 className="card-subtitle mb-2 text-muted"></h7>
                </div>
                <div>
                  <h7 className="card-subtitle mb-2 text-muted">Location: {hats.location.closet_name}</h7>
                </div>
                <div>
                  <h7 className="card-subtitle mb-2 text-muted">Section {hats.location.section_number} - Shelf {hats.location.shelf_number}</h7>
                </div>
                <button className="btn btn-primary" value={hats.href} onClick={handleRemove}>Remove</button>

                </div>
            </div>
            );
        })}
    </div>
    );
  }


  useEffect(() => {
    fetchData()
  }, [])

    return (
      <>
        <div className="text-center">
          <h1 className="display-5 fw-bold">All of your hats!</h1>
          <div className="col-lg-6 mx-auto">
          </div>
        </div>
        <div className="container">
          <div className="row">
            {hatColumns.map((hatList, index) => {
              return (
                // add props to use inside html components. key={index} varName={varName} functionName={functionName}
                <HatColumn key={index} list={hatList} handleRemove={handleRemove} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
