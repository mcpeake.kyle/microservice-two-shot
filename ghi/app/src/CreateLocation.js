import React, { useState } from 'react'

export default function NewLocation() {
    const [formData, setFormData] = useState({
        closet_name: '',
        section_number: '',
        shelf_number: '',
    })

    const handleInputChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = 'http://localhost:8100/api/locations/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFormData({
                closet_name: '',
                section_number: '',
                shelf_number: '',
            })
        }
    }
    return (
        <main>
            <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input required type="text" name="closet_name" id="closet_name" className="form-control"
                        value={formData.closet_name} onChange={handleInputChange} />
                        <label htmlFor="closet_name">Closet name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input required type="text" name="section_number" id="section_number" className="form-control"
                        value={formData.section_number} onChange={handleInputChange} />
                        <label htmlFor="section_number">Section number</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input required type="text" name="shelf_number" id="shelf_number" className="form-control"
                        value={formData.shelf_number} onChange={handleInputChange} />
                        <label htmlFor="shelf_number">Shelf number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </main>
            );
}
