# Wardrobify

Team:

* Kyle McPeake - Hats
* Jaron Laquindanum - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.


## Hats microservice
The LocationVO model was created to poll location data from the wardrobe microservice.
Hats model was created to store "style_name", "fabric", "color", "picture_url", and location (a ForeignKey to the LocationVO).
